#!/bin/sh

TFTP_DIR=/var/lib/tftpboot
HTTP_DIR=/var/www

# ---------- GETTING ISO ----------

# extract filename from url
ISO=$( echo $ISO_URL | awk -F "/" '{print $NF}' )
ISO_L="/storage/$ISO"

# if required iso is not present download it.
if [ ! -f "$ISO_L" ]; then
	echo -e "❌ $ISO is not present\n📥 Downloading"
	# TODO - check with SHA SUM
	wget -P /storage/ $ISO_URL
else
	echo "✅ $ISO is present"
fi

# symlink iso to www folder
ln -s $ISO_L /var/www
echo "✅ ISO symlink is present."

# create and link directory for kernel and intrd
mkdir -p $HTTP_DIR/os
echo "✅ required dirs and symlinks are present."

# get vmlinuz and initrd from iso
for file in vmlinuz initrd
do
	# locate the file and write its location into variable
	file_location=`7z l $ISO_L | grep $file | awk -F " " '{print $NF}'`
	# extract file from ISO to given location
	7z e $ISO_L $file_location -o$HTTP_DIR/os
done
echo "✅ vmlinuz and initrd are present."


# ---------- AUTOINSTALL FILES ----------

if [ ! -f "./storage/user-data" ]; then
	echo -e "❌ user-data is not present\n✍️ disabling autoinstall"
	sed -i "s/DEFAULT/DISABLE/g" /var/lib/tftpboot/pxelinux.cfg/default
else
	# symlink user-data to /var/www
	ln -s /storage/user-data /var/www/cloud-init/
	echo "✅ user-data symlink is present."
fi


# ---------- CONFIGURING SERVICES ----------

# set ip address
[ -z "$IP_ADDR" ] && IP_ADDR=$(hostname -i)
IP_NET=$(echo "$IP_ADDR" | awk -F. '{print $1"."$2"."$3".0"}')

# update ip addresses
sed -i "s/<ip_addr>/$IP_ADDR/g" /etc/apache2/httpd.conf
sed -i "s/<ip_addr>/$IP_ADDR/g" /etc/dnsmasq.d/netboot.conf
sed -i "s/<ip_net>/$IP_NET/g" /etc/dnsmasq.d/netboot.conf
sed -i "s/<ip_addr>/$IP_ADDR/g" /var/www/ipxe/*
sed -i "s/<ISO>/$ISO/g" /var/www/ipxe/*
echo "✅ variables in config files are set."


# ---------- STARTING SERVICES ----------
httpd
dnsmasq 
echo "😈 Services started."
sleep infinte
